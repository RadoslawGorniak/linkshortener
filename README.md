# LinkShortener

Web Service using Java/Spring for shortening links.

## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)
* [License](#license)


## General info

Website for shortening links and giving yours own names. It
allows you to monitor the number of clicks on the link.
You can see how works application in heroku: [link-shortenerr.herokuapp.com](https://link-shortenerr.herokuapp.com/)


## Technologies

* Java 11
* Spring 
* Hibernate
* Liquibase
* MySql
* H2
* HTML
* CSS
* Bootstrap 4
* JUnit 5
* Mockito 2
* Jacoco
* REST
* SOLID

## Setup

Clone the repository on your computer.
Go from the terminal to the directory where the downloaded repository is located and execute the following commands

```bash
mvnw clean install  
mvnw spring-boot:run
```

Then the application is available under [localhost:8080/](http://localhost:8080/)

## Features

List of features ready and TODOs for future development
* Shorten Link
* The user can give the link an alias/name 
* Added the details section
* Added the remove section
* Added count of clicks per link 
* Each link has a unique allias/name

To-do list:
* Addition click preview for a given day / month
* Downloading user's IP after clicking the link

## Status

Project is: In progress

## Contact

Created by [RadosławGórniak](https://www.linkedin.com/in/radoslaw-gorniak-b6b4b2172/) - feel free to contact me!

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)