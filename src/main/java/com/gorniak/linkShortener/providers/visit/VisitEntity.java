package com.gorniak.linkShortener.providers.visit;

import com.gorniak.linkShortener.providers.link.LinkEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "visits")
public class VisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int count;
    private LocalDateTime lastVisit;
    private LocalDateTime firstVisit;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "link_id", nullable = false)
    private LinkEntity link;

    public VisitEntity() {
    }

    public int getCount() {
        return count;
    }

    public LocalDateTime getLastVisit() {
        return lastVisit;
    }

    public LocalDateTime getFirstVisit() {
        return firstVisit;
    }
}
