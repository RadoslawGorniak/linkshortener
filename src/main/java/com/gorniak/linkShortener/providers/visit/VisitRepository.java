package com.gorniak.linkShortener.providers.visit;

import java.util.Optional;

public interface VisitRepository {

    VisitEntity save(VisitEntity visitEntity);

    Optional<VisitEntity> findByLinkId(Long linkId);

}
