package com.gorniak.linkShortener.providers.visit;

import com.gorniak.linkShortener.visit.Visit;
import org.modelmapper.ModelMapper;

public class VisitProvider {

    private VisitRepository visitRepository;
    private ModelMapper modelMapper;

    public VisitProvider(VisitRepository visitRepository, ModelMapper modelMapper) {
        this.visitRepository = visitRepository;
        this.modelMapper = modelMapper;
    }

    public Visit saveVisit(Visit visit) {
        VisitEntity visitEntity = modelMapper.map(visit, VisitEntity.class);
        VisitEntity visitEntitySave = visitRepository.save(visitEntity);
        return modelMapper.map(visitEntitySave, Visit.class);
    }

}
