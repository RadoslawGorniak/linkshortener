package com.gorniak.linkShortener.providers.visit;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VisitRepositorySql extends VisitRepository, JpaRepository<VisitEntity, Long> {
}