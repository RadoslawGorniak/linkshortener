package com.gorniak.linkShortener.providers.link;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface LinkRepositorySql extends LinkRepository, JpaRepository<LinkEntity, Long> {
}
