package com.gorniak.linkShortener.providers.link;

import com.gorniak.linkShortener.infrastructure.exception.LinkNotExist;
import com.gorniak.linkShortener.link.Link;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LinkProvider {

    private static final Logger logger = LoggerFactory.getLogger(LinkProvider.class);

    private final LinkRepository linkRepository;
    private final ModelMapper modelMapper;

    public LinkProvider(LinkRepository linkRepository, ModelMapper modelMapper) {
        this.linkRepository = linkRepository;
        this.modelMapper = modelMapper;
    }

    public Link save(Link link) {
        LinkEntity linkEntity = modelMapper.map(link, LinkEntity.class);
        LinkEntity linkEntitySaved = linkRepository.save(linkEntity);
        return modelMapper.map(linkEntitySaved, Link.class);
    }

    public Link findByLinkName(String linkName){
        LinkEntity linkEntity = linkRepository.findByLinkName(linkName).orElseThrow(LinkNotExist::new);
        return modelMapper.map(linkEntity, Link.class);
    }

    public boolean existsByLinkName(String linkName) {
        return linkRepository.existsByLinkName(linkName);
    }

    public void deleteLink(Link link) {
        LinkEntity linkEntity = modelMapper.map(link, LinkEntity.class);
        linkRepository.delete(linkEntity);
    }
}
