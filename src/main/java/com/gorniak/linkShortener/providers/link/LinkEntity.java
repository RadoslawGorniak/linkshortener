package com.gorniak.linkShortener.providers.link;

import com.gorniak.linkShortener.providers.visit.VisitEntity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "links")
public class LinkEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String destination;
    private String linkName;
    private LocalDate dateCreated;
    private int visits;
    private String deleteCode;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "link")
    private VisitEntity visit;

    public LinkEntity() {
    }

    public LinkEntity(String destination, String linkName, LocalDate dateCreated, int visits, String deleteCode) {
        this.destination = destination;
        this.linkName = linkName;
        this.dateCreated = dateCreated;
        this.visits = visits;
        this.deleteCode = deleteCode;
    }

    public Long getId() {
        return id;
    }

    public String getDestination() {
        return destination;
    }

    public String getLinkName() {
        return linkName;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public int getVisits() {
        return visits;
    }

    public String getDeleteCode() {
        return deleteCode;
    }
}
