package com.gorniak.linkShortener.providers.link;

import java.util.Optional;

public interface LinkRepository {

    Optional<LinkEntity> findByLinkName(String linkName);

    LinkEntity save(LinkEntity linkEntity);

    boolean existsByLinkName(String linkName);

    void delete(LinkEntity linkEntity);

}
