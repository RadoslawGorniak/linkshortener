package com.gorniak.linkShortener.infrastructure.exception;

import ch.qos.logback.classic.util.StatusViaSLF4JLoggerFactory;
import com.gorniak.linkShortener.link.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);
    public static final String LINK_DTO = "linkDto";
    public static final String DELETE_CODE = "deleteCode";
    public static final String LINK_NAME = "linkName";
    public static final String MESSAGE_ERROR_DELETE_CODE_IS_NOT_CORRECT = "Delete Code is not correct";
    public static final String MESSAGE_ERROR_LINK_NAME_ALREADY_USE = "Link Name already use. Please choice another name";


    @ExceptionHandler(LinkNotExist.class)
    protected String handleNameCanBeUnique(LinkNotExist ex) {
        return Page.LINK_NOT_EXIST.value();
    }
}
