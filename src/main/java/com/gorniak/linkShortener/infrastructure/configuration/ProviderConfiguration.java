package com.gorniak.linkShortener.infrastructure.configuration;

import com.gorniak.linkShortener.providers.link.LinkProvider;
import com.gorniak.linkShortener.providers.link.LinkRepository;
import com.gorniak.linkShortener.providers.visit.VisitProvider;
import com.gorniak.linkShortener.providers.visit.VisitRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProviderConfiguration {

    @Bean
    public LinkProvider linkProvider(LinkRepository linkRepository, ModelMapper modelMapper){
        return new LinkProvider(linkRepository,modelMapper);
    }

    @Bean
    public VisitProvider visitProvider(VisitRepository visitRepository, ModelMapper modelMapper){
        return new VisitProvider(visitRepository, modelMapper);
    }
}
