package com.gorniak.linkShortener.infrastructure.configuration;

import com.gorniak.linkShortener.link.Link;
import com.gorniak.linkShortener.link.LinkDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();

        mapper.addMappings(new PropertyMap<Link, LinkDto>() {
            @Override
            protected void configure() {
                map().setVisitCount(source.getVisit().getCount());
            }
        });

        mapper.getConfiguration()
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }
}
