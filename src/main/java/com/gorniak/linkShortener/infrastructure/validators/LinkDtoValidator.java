package com.gorniak.linkShortener.infrastructure.validators;

import com.gorniak.linkShortener.link.LinkDto;

import static com.gorniak.linkShortener.infrastructure.validators.InputValidator.validateObjectValue;
import static com.gorniak.linkShortener.infrastructure.validators.InputValidator.validateStringValue;

public class LinkDtoValidator {
    public static boolean validateLinkDto(LinkDto linkDto) {
        return validateObjectValue(linkDto) &&
                validateStringValue(linkDto.getLinkName()) &&
                validateStringValue(linkDto.getDestination()) &&
                LinkDestinationValidator.checkLinkDestination(linkDto.getDestination());
    }


}
