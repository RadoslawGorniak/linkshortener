package com.gorniak.linkShortener.infrastructure.validators;

import com.gorniak.linkShortener.providers.link.LinkRepository;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class LinkNameValidator implements ConstraintValidator<LinkNameConstraint, String> {

    private final LinkRepository linkRepository;

    public LinkNameValidator(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public void initialize(LinkNameConstraint constraintAnnotation) {}

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !linkRepository.existsByLinkName(s);
    }
}
