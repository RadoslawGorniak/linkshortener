package com.gorniak.linkShortener.infrastructure.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class LinkDestinationValidator implements
        ConstraintValidator<LinkDestinationConstraint, String> {

    private static final String HTTP_WITH_WWW_PATTERN = "^http:\\/\\/www\\.\\w+\\..*$";
    private static final String HTTP_SIMPLE_PATTERN = "^http:\\/\\/\\w+\\..*$";
    private static final String ONLY_WWW_PATTERN = "^www\\.\\w+\\..*$";
    private static final String SIMPLE_URL_PATTERN = "^\\w+\\..*$";
    private static final String HTTPS_WITH_WWW_PATTERN = "^https:\\/\\/www\\.\\w+\\..*$";
    private static final String HTTPS_SIMPLE_PATTERN = "^https:\\/\\/\\w+\\..*$";

    private static final String LINK_PATTERN =
            HTTP_SIMPLE_PATTERN
                    + '|'
                    + HTTP_WITH_WWW_PATTERN
                    + '|'
                    + ONLY_WWW_PATTERN
                    + '|'
                    + SIMPLE_URL_PATTERN
                    + '|'
                    + HTTPS_WITH_WWW_PATTERN
                    + '|'
                    + HTTPS_SIMPLE_PATTERN;

    private static final Pattern pattern = Pattern.compile(LINK_PATTERN);

    private LinkDestinationValidator() {
    }

    public static boolean checkLinkDestination(String linkDestination) throws RuntimeException{
       return pattern.matcher(linkDestination).matches();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return checkLinkDestination(s) && InputValidator.validateStringValue(s);
    }

    @Override
    public void initialize(LinkDestinationConstraint constraintAnnotation) {}
}
