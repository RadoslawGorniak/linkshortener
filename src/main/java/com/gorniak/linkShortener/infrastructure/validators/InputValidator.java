package com.gorniak.linkShortener.infrastructure.validators;


public class InputValidator {
    private InputValidator() {
    }

    public static boolean validateStringValue(String value) throws RuntimeException {
        return !value.isEmpty();
    }

    public static boolean validateObjectValue(Object value) throws RuntimeException {
        return !(value == null);
    }
}
