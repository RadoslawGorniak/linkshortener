package com.gorniak.linkShortener.infrastructure.validators;



import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = LinkDestinationValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface LinkDestinationConstraint {
    String message() default "Invalid link destination";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
