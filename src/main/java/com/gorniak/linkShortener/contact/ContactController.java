package com.gorniak.linkShortener.contact;

import com.gorniak.linkShortener.link.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/contact"})
public class ContactController {

    @GetMapping
    public String contact() {
        return Page.CONTACT.value();
    }

}
