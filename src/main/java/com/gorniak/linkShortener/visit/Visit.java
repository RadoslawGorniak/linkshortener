package com.gorniak.linkShortener.visit;


import com.gorniak.linkShortener.link.Link;

import java.time.LocalDateTime;

public class Visit {

    private Long id;
    private int count;
    private LocalDateTime lastVisit;
    private LocalDateTime firstVisit;
    private Link link;

    public Visit() {
        this.count = 0;
        this.lastVisit = LocalDateTime.now();
        this.firstVisit = LocalDateTime.now();
    }

    public Visit(Link link) {
        this();
        this.link = link;
    }

    public int getCount() {
        return count;
    }

    public LocalDateTime getLastVisit() {
        return lastVisit;
    }

    public LocalDateTime getFirstVisit() {
        return firstVisit;
    }

    public void addVisit() {
        this.count++;
    }

    public void changeLastVisit() {
        this.lastVisit = LocalDateTime.now();
    }
}
