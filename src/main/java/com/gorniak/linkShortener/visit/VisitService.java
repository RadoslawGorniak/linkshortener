package com.gorniak.linkShortener.visit;

import com.gorniak.linkShortener.link.Link;
import com.gorniak.linkShortener.providers.visit.VisitProvider;
import org.springframework.stereotype.Service;

@Service
public class VisitService {

    private VisitProvider visitProvider;

    public VisitService(VisitProvider visitProvider) {
        this.visitProvider = visitProvider;
    }

    public Visit createVisit(Link link) {
        return new Visit(link);
    }

    public void refreshVisit(Visit visit) {
        visit.addVisit();
        visit.changeLastVisit();
        visitProvider.saveVisit(visit);
    }
}
