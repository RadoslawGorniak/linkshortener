package com.gorniak.linkShortener.features;

import com.gorniak.linkShortener.link.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/features"})
public class FeaturesController {

    @GetMapping("/features")
    public String features() {
        return Page.FEATURES.value();
    }
}
