package com.gorniak.linkShortener.link;

import java.util.Random;

public class LinkDeleteCodeGenerator {

    private static final Random random = new Random();
    private static final int ONLY_DIGITS = 9;

    private LinkDeleteCodeGenerator(){}

    public static String generateDeleteCode() {
        StringBuilder deleteCode = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            deleteCode.append(random.nextInt(ONLY_DIGITS));
        }
        return deleteCode.toString();
    }
}
