package com.gorniak.linkShortener.link;

public enum Page {
    HOME_PAGE("index"),
    CONTACT("contact"),
    FEATURES("features"),
    LINK_DETAILS("linkDetails"),
    LINK_NOT_EXIST("linkNotExist"),
    REMOVE_LINK("removeLink"),
    REMOVE_LINK_SUCCESS("removeLinkSuccess"),
    ERROR("error"),
    CREATED_LINK("createdLink");

    private String page;

    Page(String page) {
        this.page = page;
    }

    public String value() {
        return page;
    }
}
