package com.gorniak.linkShortener.link;

public class LinkHttpStatement {
    private static final String HTTP = "http";
    private static final String FULL_HTTP_STATEMENT = "http://";

    private LinkHttpStatement(){}

    public static String addHttpStatement(String destination) {
        if (!destination.startsWith(HTTP)) {
            destination = FULL_HTTP_STATEMENT + destination;
        }
        return destination;
    }
}
