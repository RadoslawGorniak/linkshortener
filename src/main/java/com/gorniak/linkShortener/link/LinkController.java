package com.gorniak.linkShortener.link;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Destination;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping(value = {"/links", "/"})
public class LinkController {

    public static final String LINK_DTO = "linkDto";
    public static final String DELETE_CODE = "deleteCode";
    public static final String MESSAGE_ERROR_DELETE_CODE_IS_NOT_CORRECT = "Delete Code is not correct";
    private final LinkService linkService;
    private final ModelMapper modelMapper;

    public LinkController(LinkService linkService, ModelMapper modelMapper) {
        this.linkService = linkService;
        this.modelMapper = modelMapper;
    }

    @GetMapping()
    public String home(Model model) {
        model.addAttribute(LINK_DTO, new LinkDto());
        return Page.HOME_PAGE.value();
    }

    @PostMapping()
    public String makeLinkShort(@ModelAttribute(LINK_DTO) @Valid LinkDto linkDto, BindingResult bindingResult, Model model, HttpServletResponse httpServletResponse) {
        if (bindingResult.hasErrors()) {
            return Page.HOME_PAGE.value();
        }
        Link link = linkService.createShortedLink(convertLinkDtoToLink(linkDto));
        linkDto = convertLinkToLinkDto(link);
        model.addAttribute(LINK_DTO, linkDto);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
        return Page.CREATED_LINK.value();
    }

    @GetMapping("/{linkName}/details")
    public String getLinkDetails(@PathVariable String linkName, Model model, HttpServletResponse httpServletResponse) {
        checkLinkExists(linkName);
        Link link = linkService.getLink(linkName);
        LinkDto linkDto = convertLinkToLinkDto(link);
        httpServletResponse.setStatus(HttpServletResponse.SC_FOUND);
        model.addAttribute(LINK_DTO, linkDto);
        return Page.LINK_DETAILS.value();
    }

    @GetMapping("/{linkName}")
    public String goToLinkDestination(@PathVariable String linkName, HttpServletResponse httpServletResponse) {
        checkLinkExists(linkName);
        Link link = linkService.goToLinkDestination(linkName);
        LinkDto linkDto = convertLinkToLinkDto(link);
        httpServletResponse.setStatus(HttpServletResponse.SC_FOUND);
        return String.format("redirect:%s", linkDto.getDestination());
    }

    @GetMapping("/{linkName}/removing")
    public String toRemoveLinkPage(@PathVariable String linkName, Model model, HttpServletResponse httpServletResponse) {
        checkLinkExists(linkName);
        LinkDto linkDto = new LinkDto(linkName);
        model.addAttribute(LINK_DTO, linkDto);
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        return Page.REMOVE_LINK.value();
    }

    @PostMapping("/{linkName}/removing")
    public String removeLink(@ModelAttribute(LINK_DTO) LinkDto linkDto, BindingResult bindingResult) {
        checkLinkExists(linkDto.getLinkName());
        if (bindingResult.hasErrors()) {
            return Page.REMOVE_LINK.value();
        }
        Link link = convertLinkDtoToLink(linkDto);
        return checkDeleteCodeIsCorrect(link, bindingResult);
    }

    private String checkDeleteCodeIsCorrect(Link link, BindingResult bindingResult) {
        if (!linkService.removeLink(link)) {
            bindingResult.addError(new FieldError(LINK_DTO, DELETE_CODE,
                    MESSAGE_ERROR_DELETE_CODE_IS_NOT_CORRECT));
            return Page.REMOVE_LINK.value();
        }
        return Page.REMOVE_LINK_SUCCESS.value();
    }

    private void checkLinkExists(String linkName) {
        linkService.checkLinkExists(linkName);
    }

    private LinkDto convertLinkToLinkDto(Link link) {
        return modelMapper.map(link, LinkDto.class);
    }

    private Link convertLinkDtoToLink(LinkDto linkDto) {
        return modelMapper.map(linkDto, Link.class);
    }
}
