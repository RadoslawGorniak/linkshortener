package com.gorniak.linkShortener.link;

import com.gorniak.linkShortener.visit.Visit;

import java.time.LocalDate;
import java.util.Objects;

public class Link {

    private Long id;
    private String destination;
    private String linkName;
    private LocalDate dateCreated;
    private int visits;
    private String deleteCode;
    private Visit visit;

    public Link() {
        this.dateCreated = LocalDate.now();
        this.visits = 0;

    }

    public Link(String linkName) {
        this();
        this.linkName = linkName;
    }

    public Link(String linkName, String destination) {
        this(linkName);
        this.destination = destination;
    }
    public Link(String linkName, String destination, String deleteCode) {
        this(linkName, destination);
        this.deleteCode = deleteCode;

    }

    public Long getId() {
        return id;
    }

    public String getDestination() {
        return destination;
    }

    public String getLinkName() {
        return linkName;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public int getVisits() {
        return visits;
    }

    public String getDeleteCode() {
        return deleteCode;
    }

    public Visit getVisit() {
        return visit;
    }

    public void initialVisit(Visit visit) {
        this.visit = visit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Link link = (Link) o;
        return visits == link.visits &&
                Objects.equals(destination, link.destination) &&
                Objects.equals(linkName, link.linkName) &&
                Objects.equals(dateCreated, link.dateCreated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, linkName, dateCreated, visits, deleteCode);
    }

    @Override
    public String toString() {
        return "Link{" +
                "id=" + id +
                ", destination='" + destination + '\'' +
                ", linkName='" + linkName + '\'' +
                ", dateCreated=" + dateCreated +
                ", visits=" + visits +
                ", deleteCode='" + deleteCode + '\'' +
                ", visit=" + visit +
                '}';
    }
}

