package com.gorniak.linkShortener.link;

import com.gorniak.linkShortener.infrastructure.validators.LinkDestinationConstraint;
import com.gorniak.linkShortener.infrastructure.validators.LinkNameConstraint;

import java.time.LocalDate;

public class LinkDto {

    @LinkDestinationConstraint
    private String destination;
    @LinkNameConstraint
    private String linkName;
    private LocalDate dateCreated;
    private int visits;
    private String deleteCode;
    private int visitCount;

    public LinkDto() {
    }

    public LinkDto(String linkName) {
        this.linkName = linkName;
    }

    public LinkDto(String linkName, String destination) {
        this(linkName);
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public String getLinkName() {
        return linkName;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public int getVisits() {
        return visits;
    }

    public String getDeleteCode() {
        return deleteCode;
    }

    public int getVisitCount() {
        return visitCount;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public void setDeleteCode(String deleteCode) {
        this.deleteCode = deleteCode;
    }

    public void setVisitCount(int visitCount) {
        this.visitCount = visitCount;
    }

    @Override
    public String toString() {
        return "LinkDto{" +
                "destination='" + destination + '\'' +
                ", linkName='" + linkName + '\'' +
                ", dateCreated=" + dateCreated +
                ", visits=" + visits +
                ", deleteCode='" + deleteCode + '\'' +
                ", visitCount=" + visitCount +
                '}';
    }
}
