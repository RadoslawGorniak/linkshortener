package com.gorniak.linkShortener.link;

import com.gorniak.linkShortener.infrastructure.exception.LinkNotExist;
import com.gorniak.linkShortener.providers.link.LinkProvider;
import com.gorniak.linkShortener.visit.Visit;
import com.gorniak.linkShortener.visit.VisitService;
import org.springframework.stereotype.Service;

import static com.gorniak.linkShortener.link.LinkDeleteCodeGenerator.*;
import static com.gorniak.linkShortener.link.LinkHttpStatement.addHttpStatement;

@Service
public class LinkService {
    private final LinkProvider linkProvider;
    private final VisitService visitService;

    public LinkService(LinkProvider linkProvider, VisitService visitService) {
        this.linkProvider = linkProvider;
        this.visitService = visitService;
    }

    public Link createShortedLink(Link link) {
        link = createLink(link.getLinkName(), link.getDestination());
        Visit visit = visitService.createVisit(link);
        link.initialVisit(visit);
        return saveLink(link);
    }

    public Link getLink(String linkName) {
        return linkProvider.findByLinkName(linkName);
    }

    public Link goToLinkDestination(String linkName) {
        Link link = getLink(linkName);
        refreshVisit(link);
        return link;
    }

    public void checkLinkExists(String linkName) {
         if (!linkProvider.existsByLinkName(linkName)){
             throw new LinkNotExist();
         }
    }

    public boolean removeLink(Link linkToDelete) {
        Link link = getLink(linkToDelete.getLinkName());
        return deleteCodeIsEquals(link, linkToDelete);
    }

    private void refreshVisit(Link link) {
        visitService.refreshVisit(link.getVisit());
    }

    private Link createLink(String linkName, String linkDestination) {
        String destination = addHttpStatement(linkDestination);
        String deleteCode = generateDeleteCode();
        return new Link(linkName, destination, deleteCode);
    }

    private boolean deleteCodeIsEquals(Link link, Link linkToDelete) {
        if (!link.getDeleteCode().equals(linkToDelete.getDeleteCode())){
            return false;
        }
        linkProvider.deleteLink(link);
        return true;
    }

    private Link saveLink(Link link) {
        return linkProvider.save(link);
    }
}
