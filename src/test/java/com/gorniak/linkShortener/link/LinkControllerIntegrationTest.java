package com.gorniak.linkShortener.link;

import com.gorniak.linkShortener.providers.link.LinkEntity;
import com.gorniak.linkShortener.providers.link.LinkRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class LinkControllerIntegrationTest {

    static final String LINK_DESTINATION_VALUE = "https://www.wp.pl/";
    static final String LINK_NAME_VALUE = "linkNameTest";
    static final String LINK_NAME_VALUE2 = "linkNameTest2";
    static final String URL_MAKE_LINK_SHORT = "/links";
    static final String URL_LINK_DETAILS_FOUND = "/links/linkNameTest2/details";
    static final String URL_LINK_DETAILS_NOT_FOUND = "/links/linkNameTestNotFound/details";
    static final String URL_LINK_REMOVE = "/links/linkNameTest2/removing";
    static final String LINK_NAME = "linkName";
    static final String DESTINATION = "destination";
    static final String CONTENT_TYPE_VALUE = "text/html;charset=UTF-8";
    static final String DELETE_CODE = "deleteCode";
    static final String DELETE_CODE_VALUE = "1234";
    static final int VISITS_VALUE = 0;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LinkRepository linkRepository;

    @Test
    void httpPostMakeLinkShortSuccessful() throws Exception {
        //given
        MockHttpServletRequestBuilder createMessage = post(URL_MAKE_LINK_SHORT)
                .param(LINK_NAME, LINK_NAME_VALUE)
                .param(DESTINATION, LINK_DESTINATION_VALUE);

        //when + then
        mockMvc.perform(createMessage)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(CONTENT_TYPE_VALUE))
                .andExpect(view().name("createdLink"))
                .andExpect(handler().methodName("makeLinkShort"));
    }

    @Test
    void httpGetLinkDetailsSuccessful() throws Exception {
        //given
        LinkEntity linkEntity = new LinkEntity(LINK_DESTINATION_VALUE, LINK_NAME_VALUE2, LocalDate.now(), VISITS_VALUE, DELETE_CODE_VALUE);
        linkRepository.save(linkEntity);
        MockHttpServletRequestBuilder createMessageFound = get(URL_LINK_DETAILS_FOUND);

        //when+then
        mockMvc.perform(createMessageFound)
                .andExpect(status().isFound())
                .andExpect(view().name("linkDetails"))
                .andExpect(handler().methodName("getLinkDetails"));
    }

    @Test
    void httpGetLinkDetailsLinkNotExist() throws Exception {
        //given
        MockHttpServletRequestBuilder createMessageFound = get(URL_LINK_DETAILS_NOT_FOUND);

        //when+then
        mockMvc.perform(createMessageFound)
                .andExpect(view().name("linkNotExist"))
                .andExpect(handler().methodName("getLinkDetails"));
    }
}

