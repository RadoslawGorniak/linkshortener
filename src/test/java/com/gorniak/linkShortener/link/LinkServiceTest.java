package com.gorniak.linkShortener.link;

import com.gorniak.linkShortener.infrastructure.exception.LinkNotExist;
import com.gorniak.linkShortener.providers.link.LinkProvider;
import com.gorniak.linkShortener.providers.visit.VisitRepository;
import com.gorniak.linkShortener.visit.Visit;
import com.gorniak.linkShortener.visit.VisitService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class LinkServiceTest {

    static final String LINK_DESTINATION = "https://www.wp.pl/";
    static final String LINK_NAME = "linkNameTest";
    static final int ONE = 1;
    static final String DELETE_CODE = "1111";
    static final String DELETE_CODE_ANOTHER = "2222";

    @Mock
    private LinkProvider linkProvider;

    @Mock
    private VisitService visitService;

    @InjectMocks
    private LinkService linkService;

    private Link link;


    @BeforeEach
    public void setup() {
        link = new Link(LINK_NAME, LINK_DESTINATION, DELETE_CODE);
    }

    @Test
    void createShortedLinkFromDestinationLink() {
        //given
        when(linkProvider.save(any(Link.class))).thenReturn(link);

        //when
        Link shortedLink = linkService.createShortedLink(link);

        //then
        verify(linkProvider, times(ONE)).save(any(Link.class));
        assertThat(shortedLink.equals(link)).isTrue();
    }

    @Test
    void getLinkReturnLink() {
        //given
        when(linkProvider.findByLinkName(LINK_NAME)).thenReturn(link);

        //when
        Link linkFind = linkService.getLink(LINK_NAME);

        //then
        assertThat(linkFind.equals(link)).isTrue();
    }

    @Test
    void redirectToShouldReturnLink() {
        //given
        when(linkProvider.findByLinkName(LINK_NAME)).thenReturn(link);

        //when
        Link linkWithUpperVisits = linkService.goToLinkDestination(LINK_NAME);

        //then
        assertThat(linkWithUpperVisits.equals(link)).isTrue();
    }


    @Test
    void checkLinkExistsShouldThrowExceptionWhenLinkAlreadyExist() {
        //when
        when(linkProvider.existsByLinkName(LINK_NAME)).thenReturn(false);

        //when + then
        assertThatExceptionOfType(LinkNotExist.class)
                .isThrownBy(() -> linkService.checkLinkExists(LINK_NAME));

    }

    @Test
    void removeLinkShouldReturnTrueWhenLinkNameExistAndCodesDeleteIsTheSame() {
        //given
        Link linkToDelete = new Link(LINK_NAME, LINK_DESTINATION, DELETE_CODE);
        when(linkProvider.findByLinkName(LINK_NAME)).thenReturn(link);

        //when
        boolean isRemoveLink = linkService.removeLink(linkToDelete);

        //then
        assertThat(isRemoveLink).isTrue();
        verify(linkProvider, times(1)).deleteLink(any(Link.class));
    }

    @Test
    void removeLinkShouldReturnFalseWhenDeleteCodesInNotEquals() {
        //given
        Link linkToDelete = new Link(LINK_NAME, LINK_DESTINATION, DELETE_CODE_ANOTHER);
        when(linkProvider.findByLinkName(LINK_NAME)).thenReturn(link);

        //when
        boolean isRemoveLink = linkService.removeLink(linkToDelete);

        //then
        assertThat(isRemoveLink).isFalse();
        verify(linkProvider, times(0)).deleteLink(any(Link.class));
    }

}