package com.gorniak.linkShortener.link;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class LinkTest {

    static final String LINK_DESTINATION = "https://www.wp.pl/";
    static final String LINK_NAME = "linkNameTest";
    static final int FOUR = 4;
    public static final String LINK_NAME_2 = "linkNameTes2";
    public static final String LINK_DESTINATION_2 = "linkDestinationTest2";

    private Link link;


    @BeforeEach
    void setUp() {
        link = new Link(LINK_NAME, LINK_DESTINATION);
    }


    @Test
    void methodEqualsShouldReturnTrueWhenObjectsHaveTheSameValueOfVariables() {
        //given
        Link linkSecond = new Link(LINK_NAME, LINK_DESTINATION);

        //when
        boolean isEquals = link.equals(linkSecond);

        //then
        assertAll(
                () -> assertThat(link.getLinkName()).isEqualTo(linkSecond.getLinkName()),
                () -> assertThat(link.getVisits()).isEqualTo(linkSecond.getVisits()),
                () -> assertThat(link.getDateCreated()).isEqualTo(linkSecond.getDateCreated()),
                () -> assertThat(link.getDestination()).isEqualTo(linkSecond.getDestination())
        );
    }

    @Test
    void methodEqualsShouldReturnTrueWhenObjectsIsTheSameEquals(){
        //given
        Link linkSecond = link;

        //when
        boolean isEquals = linkSecond.equals(link);

        //then
        assertThat(isEquals).isTrue();
    }

    @Test
    void methodEqualsShouldReturnFalseWhenObjectsHaveDifferentValueOfVariables(){
        //given
        Link linkSecond = new Link(LINK_NAME_2, LINK_DESTINATION_2);

        //when
        boolean isEquals = link.equals(linkSecond);

        //then
        assertThat(isEquals).isFalse();
        assertAll(
                () -> assertThat(link.getLinkName()).isNotEqualTo(linkSecond.getLinkName()),
                () -> assertThat(link.getDestination()).isNotEqualTo(linkSecond.getDestination())
        );
    }
}
