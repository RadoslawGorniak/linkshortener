package com.gorniak.linkShortener.visit;

import com.gorniak.linkShortener.providers.visit.VisitProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
public class VisitServiceTest {

    @Mock
    private VisitProvider visitProvider;

    @InjectMocks
    private VisitService visitService;

    @Test
    void refreshVisitShouldRefreshVisitFields(){
        //given
        Visit visit = new Visit();
        int count = visit.getCount();
        LocalDateTime lastVisitDate = visit.getLastVisit().minusHours(1);

        //when
        visitService.refreshVisit(visit);

        //then
        assertThat(visit.getCount()).isGreaterThan(count);
        assertThat(visit.getLastVisit()).isAfter(lastVisitDate);
    }
}
