package com.gorniak.linkShortener.infrastructure.validators;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class LinkDestinationValidatorTest {

    static final String LINK_EMPTY = "";
    static final String LINK_INVALID = "linkInvalid";
    static final String LINK_WITH_WWW = "www.wp.pl";
    static final String LINK_WITH_HTTPS = "https://gitlab.com/";
    static final String LINK_WITH_HTTP = "http://www.example.com";
    static final String LINK_SIMPLE = "www.google.pl";
    static final String LINK_WITH_HTTP_AND_WWW = "http://www.google.pl";
    static final String LINK_WITH_HTTPS_AND_WWW = "https://www.google.pl";

    @Test
    void checkLinkShouldThrowExceptionWhenLinkNotMatchesPattern() {
        //given + when + then
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_INVALID)).isFalse();
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_EMPTY)).isFalse();
    }

    @Test
    void checkLinkShouldDoesNotThrowExceptionWhenLinkIsMatchesPattern() {
        //given + when + then
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_WITH_WWW)).isTrue();
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_WITH_HTTPS)).isTrue();
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_WITH_HTTP)).isTrue();
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_SIMPLE)).isTrue();
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_WITH_HTTP_AND_WWW)).isTrue();
        assertThat(LinkDestinationValidator.checkLinkDestination(LINK_WITH_HTTPS_AND_WWW)).isTrue();
    }
}
