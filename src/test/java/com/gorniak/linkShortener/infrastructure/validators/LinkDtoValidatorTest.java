package com.gorniak.linkShortener.infrastructure.validators;

import com.gorniak.linkShortener.link.LinkDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
public class LinkDtoValidatorTest {

    static final String LINK_DESTINATION = "https://www.wp.pl/";
    static final String LINK_NAME = "linkNameTest";
    static final String LINK_DESTINATION_EMPTY = "";
    static final String LINK_NAME_EMPTY = "";


    private LinkDto linkDto;

    @Test
    void inputValidatorReturnFalseWhenObjectIsNull() {
        //when+then
        assertFalse(LinkDtoValidator.validateLinkDto(null));
    }

    @Test
    void inputValidatorReturnFalseWhenLinkPrototypeExistLinkNameEmptyLinkDestinationEmpty() {
        //given
        linkDto = new LinkDto(LINK_NAME_EMPTY, LINK_DESTINATION_EMPTY);

        //when+then
        assertTrue(InputValidator.validateObjectValue(linkDto));
        assertFalse(InputValidator.validateStringValue(linkDto.getDestination()));
        assertFalse(InputValidator.validateStringValue(linkDto.getLinkName()));
        assertFalse(LinkDtoValidator.validateLinkDto(linkDto));
    }

    @Test
    void inputValidatorReturnFalseWhenLinkPrototypeExistLinkNameExistLinkDestinationEmpty() {
        //given
        linkDto = new LinkDto(LINK_NAME, LINK_DESTINATION_EMPTY);

        //when+then
        assertTrue(InputValidator.validateObjectValue(linkDto));
        assertTrue(InputValidator.validateStringValue(linkDto.getLinkName()));
        assertFalse(InputValidator.validateStringValue(linkDto.getDestination()));
        assertFalse(LinkDtoValidator.validateLinkDto(linkDto));
    }

    @Test
    void inputValidatorReturnTrueWhenLinkPrototypeExistLinkNameExistLinkDestinationExist() {
        //given
        linkDto = new LinkDto(LINK_NAME, LINK_DESTINATION);

        //when+then
        assertTrue(InputValidator.validateObjectValue(linkDto));
        assertTrue(InputValidator.validateStringValue(linkDto.getDestination()));
        assertTrue(InputValidator.validateStringValue(linkDto.getLinkName()));
        assertTrue(LinkDtoValidator.validateLinkDto(linkDto));
    }

}
