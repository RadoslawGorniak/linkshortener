package com.gorniak.linkShortener.providers.link;

import com.gorniak.linkShortener.link.Link;
import com.gorniak.linkShortener.providers.link.LinkEntity;
import com.gorniak.linkShortener.providers.link.LinkProvider;
import com.gorniak.linkShortener.providers.link.LinkRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class LinkProviderTest {

    static final String LINK_DESTINATION = "https://www.wp.pl/";
    static final String LINK_NAME = "linkNameTest";
    static final String LINK_NAME_NOT_EXIST = "linkNameNotExistTest";
    @Mock
    private LinkRepository linkRepository;

    private ModelMapper modelMapper;
    private LinkProvider linkProvider;
    private Link link;
    private LinkEntity linkEntity;

    @BeforeEach
    void setUp() {
        modelMapper = createMapper();
        linkProvider = new LinkProvider(linkRepository, modelMapper);
        link = new Link(LINK_NAME, LINK_DESTINATION);
        linkEntity = modelMapper.map(link, LinkEntity.class);
    }

    @Test
    void shouldSaveLinkToDB() {
        //given
        when(linkRepository.save(any(LinkEntity.class))).thenReturn(linkEntity);

        //when
        Link linkSaved = linkProvider.save(link);

        //then
        assertAll(
                () -> assertThat(link.getLinkName()).isEqualTo(linkSaved.getLinkName()),
                () -> assertThat(link.getDestination()).isEqualTo(linkSaved.getDestination()),
                () -> assertThat(link.getDeleteCode()).isEqualTo(linkSaved.getDeleteCode()),
                () -> assertThat(link.getVisits()).isEqualTo(linkSaved.getVisits()),
                () -> assertThat(link.getDateCreated()).isEqualTo(linkSaved.getDateCreated())
        );
    }

    @Test
    void findByLinkNameShouldReturnLink(){
        //given
        when(linkRepository.findByLinkName(LINK_NAME)).thenReturn(Optional.of(linkEntity));

        //when
        Link linkFind = linkProvider.findByLinkName(LINK_NAME);
        assertAll(
                () -> assertThat(linkEntity.getLinkName()).isEqualTo(linkFind.getLinkName()),
                () -> assertThat(linkEntity.getDestination()).isEqualTo(linkFind.getDestination()),
                () -> assertThat(linkEntity.getDeleteCode()).isEqualTo(linkFind.getDeleteCode()),
                () -> assertThat(linkEntity.getVisits()).isEqualTo(linkFind.getVisits()),
                () -> assertThat(linkEntity.getDateCreated()).isEqualTo(linkFind.getDateCreated())
        );
    }

    @Test
    void findByLinkNameShouldThrowExceptionWhenLinkWithLinkNameNotExist(){
        //given
        when(linkRepository.findByLinkName(LINK_NAME_NOT_EXIST)).thenReturn(Optional.empty());

        //when+then
        assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> linkProvider.findByLinkName(LINK_NAME_NOT_EXIST));
    }

    private ModelMapper createMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }

}
