package com.gorniak.linkShortener.providers.visit;

import com.gorniak.linkShortener.visit.Visit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class VisitProviderTest {

    @Mock
    private VisitRepository visitRepository;
    private ModelMapper modelMapper;
    private VisitProvider visitProvider;

    @BeforeEach
    void setUp() {
        modelMapper = createMapper();
        visitProvider = new VisitProvider(visitRepository, modelMapper);
    }

    @Test
    void saveVisitShouldReturnVisitEqualsVisitEntity() {
        //given
        Visit visit = new Visit();
        VisitEntity visitEntity = modelMapper.map(visit, VisitEntity.class);
        when(visitRepository.save(any(VisitEntity.class))).thenReturn(visitEntity);
        //when
        Visit saveVisit = visitProvider.saveVisit(visit);

        //then
        assertThat(visitEntity.getCount()).isEqualTo(saveVisit.getCount());
        assertThat(visitEntity.getFirstVisit()).isEqualTo(saveVisit.getFirstVisit());
        assertThat(visitEntity.getLastVisit()).isEqualTo(saveVisit.getLastVisit());
    }


    private ModelMapper createMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }
}
