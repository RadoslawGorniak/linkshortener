 - [0.0.1]
   [#1](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/1) - Add Structure project 
 - [0.0.2]
   [#2](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/2) - Create an endpoint for shortening links 
 - [0.0.3]
   [#3](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/3) - Create view for create a shorted link 
 - [0.1.1]
   [#4](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/4) - Add endpoint and view for remove shorted link 
 - [0.1.2]
   [#5](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/5) - Added Bootstrap 4 
 - [0.1.3]
   [#6](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/6) - Connect to db (MySql or PostgreSql)
 - [1.1.3]
   [#7](https://gitlab.com/RadoslawGorniak/linkshortener/-/issues/7) - Placing the application on heroku